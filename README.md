# dcy-spring-cloud-kubernetes

java生态开源的解决方案都是spring cloud alibaba技术栈，使用nacos作为注册中心和配置中心。

本人也开源一套spring cloud alibaba+dubbo的开源框架有需要可以参考：
[https://gitee.com/dcy421/dcy-fast-cloud](https://gitee.com/dcy421/dcy-fast-cloud)

接触到kubernetes之后，就有个疑问，为什么kubernetes的service不能当注册中心，configmap不能当配置中心呢？
带着自己的以为找了spring的官网，找到了解决版本`spring-cloud-kubernetes`。把nacos完全排除了。
使用service服务发现，使用configmap+secret当配置中心。所以开源这个项目，让更多的人入门`spring-cloud-kubernetes`技术。

官方地址：[https://spring.io/projects/spring-cloud-kubernetes#support](https://spring.io/projects/spring-cloud-kubernetes#support)

# 搭建k8s集群
> 可以搭建k8s标准集群，或者搭建`minikube`开发环境。

本人16g内存，使用虚拟机方式创建 `minikube`测试。
```shell
# 安装docker 百度知道
# 安装 minikube
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube

# 安装virtualbox 和 conntrack
wget https://download.virtualbox.org/virtualbox/6.0.4/VirtualBox-6.0-6.0.4_128413_el7-1.x86_64.rpm
yum install VirtualBox-6.0-6.0.4_128413_el7-1.x86_64.rpm
yum install conntrack

# 运行
minikube start \
--image-repository=registry.cn-hangzhou.aliyuncs.com/google_containers \
--force=true \
--driver=none 

# 安装kubectl
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
kubectl version

# 测试
kubectl create deployment nginx --image=nginx
kubectl expose deployment nginx --port=80 --type=NodePort
kubectl get pod,svc

minikube service nginx --url
# 直接访问即可

```



# 开发环境运行 win
1. 在用户目录下创建 `.kube` 文件夹。目录地址：`C:\Users\Administrator\.kube`
2. 把linux目录下的 `/root/.kube` 拷贝到win主机上`C:\Users\Administrator\.kube`路径下。
3. 把对应`config` 需要的证书文件也拷贝到 该目录下。
4. 下载`kubectl.exe` win版本的，并安装环境变量。路径地址：[https://kubernetes.io/zh/docs/tasks/tools/install-kubectl-windows/](https://kubernetes.io/zh/docs/tasks/tools/install-kubectl-windows/)
5. 测试 `kubectl` 命令


# 创建部署文件
```shell
kubectl create ns demo
kubectl apply -f dcy-spring-cloud-kubernetes-configmap.yaml -n demo
kubectl apply -f provider-demo-configmap.yaml -n demo
kubectl apply -f provider-demo-secret.yaml -n demo
```

# RBAC权限
```shell
kubectl create clusterrolebinding permissive-binding \
--clusterrole=cluster-admin \
--user=admin \
--user=kubelet \
--group=system:serviceaccounts
```
> 注意：开发和测试环境使用

# build镜像
```shell
docker build -t dcy/provider-demo:v1 .
docker build -t dcy/consumer-demo:v1 .
docker build -t dcy/gateway-demo:v1 .
```


# 部署k8s服务

```shell
kubectl apply -f provider-demo.yaml -n demo
kubectl apply -f consumer-demo.yaml -n demo
kubectl apply -f gateway-demo.yaml -n demo

kubectl get po,svc -n demo
kubectl logs -f consumer-demo-xxx -n demo
kubectl logs -f provider-demo-xxx -n demo
kubectl logs -f gateway-demo-566584b46b-ndvf2 -n demo

curl http://minikube:31002/sayHello?name=xiaoming
```