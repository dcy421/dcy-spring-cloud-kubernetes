package com.dcy.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * docker build -t dcy/springcloud-kubernetes-discovery-demo:v0.0.1 .
 * docker run --name springcloud-kubernetes-discovery-demo -d -p 8080:8080 dcy/springcloud-kubernetes-discovery-demo:v0.0.1
 * kubectl delete -f springcloud-kubernetes-discovery-demo.yaml
 * <p>
 * <p>
 * kubectl create clusterrolebinding permissive-binding \
 * --clusterrole=cluster-admin \
 * --user=admin \
 * --user=kubelet \
 * --group=system:serviceaccounts
 */
@EnableScheduling
@SpringBootApplication
@EnableDiscoveryClient
public class ProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProviderApplication.class, args);
    }
}