package com.dcy.provider.configmap;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

@Data
@RefreshScope
@Configuration
@ConfigurationProperties(prefix = "config")
public class ConfigRead {

    private String stringValue;
    private Integer numberValue;
    private boolean booleanValue;

}
