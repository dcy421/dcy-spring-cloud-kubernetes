package com.dcy.provider.controller;

import com.dcy.service.api.UserInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
public class ServiceController implements UserInterface {

    @Autowired
    private DiscoveryClient discoveryClient;

    @GetMapping("/service")
    public List<String> getServiceList() {
        return discoveryClient.getServices();
    }

    @GetMapping("/instance")
    public Object getInstance(@RequestParam("name") String name) {
        return discoveryClient.getInstances(name);
    }

    @GetMapping("/sayHello")
    @Override
    public String sayHello(@RequestParam("name") String name) {
        log.info("调用提供者name {}", name);
        return "Hello name:" + name;
    }
}
