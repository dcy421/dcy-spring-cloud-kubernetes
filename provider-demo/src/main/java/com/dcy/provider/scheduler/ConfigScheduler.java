package com.dcy.provider.scheduler;

import com.dcy.provider.configmap.ConfigRead;
import com.dcy.provider.secret.SecretRead;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author ruge.wu
 * @since 2022/4/3 20:17
 **/
@Slf4j
@Component
public class ConfigScheduler {


    @Autowired
    private ConfigRead configRead;

    @Autowired
    private SecretRead secretRead;


    @Scheduled(fixedRate = 5000)
    public void reportCurrentTime() {
        log.info("configRead {}", configRead.toString());
        log.info("secretRead {}", secretRead.toString());
    }

}
