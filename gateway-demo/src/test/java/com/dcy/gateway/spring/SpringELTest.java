package com.dcy.gateway.spring;

import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.ServiceSpec;
import org.junit.jupiter.api.Test;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.SimpleEvaluationContext;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dcy
 * @description 测试
 * @createTime 2022/6/1 17:55
 */
public class SpringELTest {

    @Test
    void test1() {
        // 输入待验证的表达式
        String springEL = "metadata.namespace == 'mydlqcloud' ";
        // 创建模拟的 Service 对象
        Service service = createService();
        // 验证 SpringEL 表达式和对象中现有的值是否匹配
        boolean isTrue = verification(service, springEL);
        //输出结果
        System.out.println(isTrue);
    }

    @Test
    void test2() {
        // 输入待验证的表达式
        String springEL = "metadata.labels.app matches '^[A-Za-z]+$'";
        // 创建模拟的 Service 对象
        Service service = createService();
        // 验证 SpringEL 表达式和对象中现有的值是否匹配
        boolean isTrue = verification(service, springEL);
        //输出结果
        System.out.println(isTrue);

    }

    /**
     * 创建模拟的 Service 对象，和 SpringCloud Kubernetes 服务发现实例保持一致
     *
     * @return
     */
    static Service createService() {
        /** 设置Spec **/
        ServiceSpec serviceSpec = new ServiceSpec();
        serviceSpec.setClusterIP("10.10.1.11");
        // 设置type
        serviceSpec.setType("NodePort");
        // 设置 Selector
        Map selector = new HashMap();
        selector.put("app", "test-project");
        serviceSpec.setSelector(selector);
        /** 设置 Metadata **/
        ObjectMeta objectMeta = new ObjectMeta();
        objectMeta.setName("test-project");
        objectMeta.setNamespace("mydlqcloud");
        // 设置Label
        Map labels = new HashMap();
        labels.put("app", "test-project");
        labels.put("group", "b");
        objectMeta.setLabels(labels);
        objectMeta.setResourceVersion("3373499");
        objectMeta.setCreationTimestamp("2019-06-23T16:24:39Z");
        /** 设置Service **/
        Service service = new Service();
        service.setKind("Service");
        service.setMetadata(objectMeta);
        return service;
    }

    /**
     * 使用 SpringEL 表达式测试 filter
     *
     * @param object
     * @param springEL
     * @return
     */
    static boolean verification(Object object, String springEL) {
        // 创建上下文环境
        SimpleEvaluationContext evalCtxt = SimpleEvaluationContext.forReadOnlyDataBinding().withInstanceMethods().build();
        // 创建SpEL表达式的解析器
        ExpressionParser parser = new SpelExpressionParser();
        // 验证表达式验证内容是否和对象中的值匹配并返回 Boolean 结果
        return parser.parseExpression(springEL).getValue(evalCtxt, object, Boolean.class);
    }
}
