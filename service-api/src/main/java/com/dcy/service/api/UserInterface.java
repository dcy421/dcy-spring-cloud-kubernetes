package com.dcy.service.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author dcy
 * @description 接口
 * @createTime 2022/6/1 15:22
 */
@FeignClient(name = "http://provider-demo:8081", url = "http://provider-demo:8081")
public interface UserInterface {

    /**
     * 获取测试信息
     *
     * @return
     */
    @GetMapping("/sayHello")
    String sayHello(@RequestParam("name") String name);
}
